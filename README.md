# README #
Creado por Irene Mart�n-Maestro , Irene Mengual Mahave y Stella Moral Salguero.


### What is this repository for? ###

* Este repositorio muestra los archivos necesarios para la implementaci�n un control de luces dom�tico.
* 05/02/2021

### How do I get set up? ###

* Microprocesador utilizado : STM32F407
* Versi�n 1.5.1 STMCubeIDE


### Breve resumen de la aplicaci�n*

* El control de luces presenta un pulsador manual que activa una luz y un ventilador. 
* Pasados 20 segundos de dicha acci�n se activar� una alarma que se desactivar� de forma manual mediante el mismo pulsador.
* Simult�neamente, se encuentra un control de luz autom�tico realizado por un sensor de presencia.
* Adicionalmente se suma un ldr que, mediante la intensidad de luz entrante apagar� otra luz.
* En todo momento aparecer� una pantalla que nos da una acogedora bienvenida a nuestro sistema.


